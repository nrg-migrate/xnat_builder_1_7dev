/*
 * org.nrg.xnat.turbine.modules.actions.ManageProjectPermissions
 * XNAT http://www.xnat.org
 * Copyright (c) 2014, Washington University School of Medicine
 * All Rights Reserved
 *
 * Released under the Simplified BSD.
 *
 * Last modified 7/10/13 9:04 PM
 */
package org.nrg.xnat.turbine.modules.actions;

import org.apache.turbine.util.RunData;
import org.apache.velocity.context.Context;
import org.nrg.xdat.turbine.modules.actions.SecureAction;

public class ManageProjectPermissions extends SecureAction {

    @Override
    public void doPerform(RunData data, Context context) throws Exception {
    }

}
