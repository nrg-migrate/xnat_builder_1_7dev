The latest release for XNAT is version 1.6.3. You can find the latest release notes for XNAT at:

https://wiki.xnat.org/display/XNAT16/XNAT+1.6+Release+Notes

You can report bugs by sending an email to bugs@xnat.org.

More information is available on the XNAT discussion group on Google Groups:

https://groups.google.com/forum/#!forum/xnat_discussion

You can also correspond via the XNAT Twitter feed at:

https://twitter.com/NrgXnat

Thanks,
The XNAT development team
Neuroinformatics Research Group
Washington University School of Medicine

